package main

import (
    "net/http"
    "path/filepath"
    "github.com/gorilla/mux"
    "html/template"
    "strings"
    "io/ioutil"
    "log"
    "fmt"
    "os"
    "encoding/json"
    "github.com/rwcarlsen/goexif/exif"
)

type Metadata struct {
    ID string
    ApertureValue []string
    BrightnessValue []string
    ColorSpace []int
    DateTime string
    ExposureMode []int
    ExposureTime []string
    FocalLength []string
    ISOSpeedRatings []int
    Make string
    Model string
    ShutterSpeedValue []string
    Software string
    WhiteBalance []int
}

type Gallery struct {
    Img []string
}

func GetFiles() []string {
    var files []string
    root := "images"
    err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
        if !info.IsDir() {
	    // TODO: change this part
            path_ := strings.TrimPrefix(path, "images/")
            files = append(files, path_)
        }
        return nil
    })

    if err != nil {
	log.Println(err)
    }

    return files
}

func handlePage(w http.ResponseWriter, request *http.Request) {
    files := []string{
       "templates/header.tmpl",
       "templates/gallery.tmpl",
    }

    tmplt, _ := template.ParseFiles(files...)

    path := GetFiles()
    event := Gallery{
        Img: path,
    }

    err := tmplt.ExecuteTemplate(w, "gallery", event)
    if err != nil {
	log.Println(err)
	return
    }

    w.Header().Set("Content-Type", "text/html")
}

func image_handler(w http.ResponseWriter, r *http.Request){
    path := strings.TrimPrefix(r.URL.Path, "/image/")

    img := ReadMetadata("images/"+path)

    files := []string{
	"templates/header.tmpl",
        "templates/images.tmpl",
    }

    tmplt, _ := template.ParseFiles(files...)
    err := tmplt.ExecuteTemplate(w, "images", img)

    if err != nil {
	log.Println(err)
	return
    }

    w.Header().Set("Content-Type", "text/html")
}

func images_handler(w http.ResponseWriter, r *http.Request){
    path := strings.TrimPrefix(r.URL.Path, "/")
    buf, err := ioutil.ReadFile(path)
    if err != nil {
	http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	return
    }

    w.Header().Set("Content-Type", "image/png")
    w.Write(buf)
}

func ReadMetadata(path string) *Metadata {
    var err        error
    var imgFile    *os.File
    var metaData   *exif.Exif
    var jsonByte   []byte
    var metadata *Metadata

    imgFile, err = os.Open(path)
    if err != nil {
	log.Println(err)
    }

    metaData, err = exif.Decode(imgFile)
    if err != nil {
	log.Println(err)
    }

    jsonByte, err = metaData.MarshalJSON()
    if err != nil {
	log.Println(err)
    }

    json.Unmarshal(jsonByte, &metadata)
    metadata.ID = path

    return metadata
}

func main(){
    routes := mux.NewRouter()
    port := ":8080"
    wd, err := os.Getwd()
    if err != nil {
	log.Println(err)
    }

    routes.PathPrefix(wd+"/images").Handler(http.StripPrefix(wd+"/images", http.FileServer(http.Dir(wd+"/images"))))
    routes.PathPrefix(wd+"/static").Handler(http.StripPrefix(wd+"/static", http.FileServer(http.Dir(wd+"/static"))))
    routes.HandleFunc("/", handlePage)
    routes.HandleFunc("/image/{rest:.*}", image_handler)
    routes.HandleFunc("/images/{rest:.*}", images_handler)
    fmt.Println("Server is running", port)
    log.Fatal(http.ListenAndServe(port, routes))
}
